<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title><?php bloginfo('name'); ?></title>

    <!-- META TAGS-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Descrição da Landing Page" />
    <meta name="keywords" content="Landing Page, Roza, Artesanato" />
    <meta name="author" content="Ateliê Roza Azul" />

    <meta property="og:locale" content="pt_BR" />
    <meta property="og:url" content="url da página" />
    <meta property="og:title" content="Ateliê Roza Azul - Artesanatos em Geral" />
    <meta property="og:description" content="Descrição da Landing Page" />
    <meta property="og:site_name" content="Ateliê Roza Azul" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="assets/images/logo_share.png" />

    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <!-- FIM - META TAGS-->

    <!-- LINKS TAGS-->
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/estilo.css" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url');?>/assets/images/favicon.png" />
    <link rel="icon" type="image/x-icon" href="<?php bloginfo('template_url');?>/assets/images/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" />
    <!-- FIM - LINKS TAGS -->

     <?php wp_head(); ?>       
</head>

<body>
    <main id="website">
        <!-- INICIO TOPO -->
        <header id="topo">
            <div class="conteudo_padrao">
                <span id="logo">
                    <a href="index.html" title="Ateliê Roza Azul">Ateliê Roza Azul</a>
                </span>
            </div>
            <nav id="menu">
                <ul>
                    <li><a href="">Home</a></li>
                    <li><a href="">Sobre nós</a></li>
                    <li><a href="">Serviços</a></li>
                    <li><a href="">Contato</a></li>
                </ul>
            </nav>
            <div id="nav_hamburger">
                <span>Menu</span>
            </div>
            <nav id="nav_responsive"></nav>
        </header>

        <div class="container">
            <div class="capa">
                <div class="texto-capa">
                    <h1>Crie, transforme... faça artesanato</h1>
                </div>
            </div>
        </div>
        <!-- FIM - INICIO TOPO -->