$(document).ready(function() {
    $("#nav_responsive").html($("#menu").html());
    $("#nav_hamburger span").click(function() {
        if ($("nav#nav_responsive ul").hasClass("collapse")) {
            $("nav#nav_responsive ul.collapse").removeClass("collapse").slideUp(250);
            $(this).removeClass("abrir");
        } else {
            $("nav#nav_responsive ul").addClass("collapse").slideDown(250);
            $(this).addClass("abrir");
        }
    });
});