<?php get_header(); ?>
<!-- INICIO - CORPO -->
<section id="corpo" class="conteudo_padrao">
    <h1 class="titulo_site">Ateliê Roza Azul</h1>
    <h2 class="subtitulo_site">Novidades nos Nossos Serviços</h2>
    <ul id="lista_servicos">
        <li><img src="<?php bloginfo('template_url');?>/assets/images/servicos/servico-artg.png" alt="Artesanatos em Geral">
            <p>Artesanatos em Geral</p>
            <span>Dolor officia ea minim Lorem nulla reprehenderit velit veniam reprehenderit exercitation.</span>
        </li>
        <li>
            <img src="<?php bloginfo('template_url');?>/assets/images/servicos/servico-camiseta.png" alt="Camisetas Personalizadas">
            <p>Camisetas Personalizadas</p>
            <span>Dolor officia ea minim Lorem nulla reprehenderit velit veniam reprehenderit exercitation.</span>
        </li>
        <li>
            <img src="<?php bloginfo('template_url');?>/assets/images/servicos/servico-gift.png" alt="Presentes Artesanais">
            <p>Presentes Artesanais</p>
            <span>Dolor officia ea minim Lorem nulla reprehenderit velit veniam reprehenderit exercitation.</span>
        </li>
    </ul>
    <a href="#" title="Veja Mais dos Nossos Serviços" class="link_botao">Veja Mais dos Nossos Serviços</a><br><br>

</section>
<!-- FIM - CORPO -->

<!-- INICIO - NEWSLETTER -->
<section id="newsletter">
    <div class="container_news">
        <div class="conteudo_padrao">
            <h2>Esteja por dentro das nossas novidades!</h2>
        </div>
        <form action="" method="post" id="form_newsletter">
            <div class="content_form">
                <div class="input_form">
                    <label for="">Nome: </label>
                    <input type="text" name="nome" class="input_campo" placeholder="Digite o seu nome" required>
                </div>
                <div class="input_form">
                    <label for="">E-mail: </label>
                    <input type="email" name="email" class="input_campo" placeholder="Digite o seu e-mail" required>
                </div>
                <div class="submit">
                    <input type="submit" value="Enviar" class="input_submit">
                </div>
            </div>
        </form>
    </div>
</section>
<!-- FIM - NEWSLETTER -->
<?php get_footer(); ?>