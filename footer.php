
        <!-- INICIO - RODAPÉ -->
        <footer id="rodape">
            <div>
                <a href="https://www.facebook.com/AtelieRozaAzul/" class="rede_social rede_social_fb" target="_blank" title="Facebook">Facebook</a>
                <a href="https://www.behance.net/" class="rede_social rede_social_be" target="_blank" title="Behance">Behance</a>
                <a href="https://www.instagram.com/atelierozaazul/" class="rede_social rede_social_in" target="_blank" title="Instagram">Instagram</a>

                <p class="copyright"><?php echo date('Y'); ?> Todos os direitos reservados | © Atelie Roza Azul</p>
            </div>
        </footer>
        <!-- FIM - RODAPÉ -->
    </main>
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- scripts externos -->
    <script src="<?php bloginfo('template_url');?>/assets/js/script.js"></script>
    <?php wp_footer(); ?>
</body>

</html>